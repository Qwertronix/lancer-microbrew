
export interface NpcStats {
  activations?: number[]
  armor: number[]
  hp: number[]
  evasion: number[]
  edef: number[]
  heatcap: number[]
  speed: number[]
  sensor: number[]
  save: number[]
  hull: number[]
  agility: number[]
  systems: number[]
  engineering: number[]
  // size: number[][]
  structure?: number[]
  stress?: number[]
}
  
  export interface TagData {
    id: string
    name: string
    val?: number
    target?: string
    editing?: boolean
  }
  
  export enum NpcFeatureType {
    Trait = 'Trait',
    System = 'System',
    Reaction = 'Reaction',
    Weapon = 'Weapon',
    Tech = 'Tech',
  }
  
export class NpcFeature {
  _prefix?: string
  name: string
  trigger?: string
  effect?: string
  tags: TagData[]
  type: NpcFeatureType
  constructor(name: string, tags: TagData[] = [], type = NpcFeatureType.System) {
    this.name = name
    this.tags = tags
    this.type = type
  }
}

export class NPC {
  prefix: string;
  name: string;
  role: string;
  size: number;
  flavor: string;
  power: number;
  stats: NpcStats;
  tactics: string;
  basesystems: NpcFeature[];
  optionalsystems: NpcFeature[];
  constructor() {
    this.prefix = ""
    this.name = ""
    this.role = ""
    this.size = 1
    this.flavor = ""
    this.power = 100
    this.stats = {
        hull: [1,1,1],
        agility: [1,1,1],
        systems: [1,1,1],
        engineering: [1,1,1],
        hp: [1,1,1],
        armor: [1,1,1],
        evasion: [1,1,1],
        edef: [1,1,1],
        speed: [1,1,1],
        heatcap: [1,1,1],
        save: [1,1,1],
        sensor: [1,1,1],
    }
    this.tactics = ""
    this.basesystems = [] 
    this.optionalsystems = [] 
  }
}