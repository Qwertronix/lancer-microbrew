import { inject, provide } from "@vue/composition-api";
import { Store } from 'vuex';

export const StoreSymbol = Symbol()

export function provideStore(store: Store<unknown>) {
    provide(StoreSymbol, store)
}

export default function useStore() {
    const store = inject(StoreSymbol)
    if (!store) {
        console.warn('No store provided, cannot inject vuex store.')
    }
    return store
}