import { TagData } from '@/types';

export function useTags(): TagData[] {
    return [
        {
            "id": "tg_burn",
            "name": "BURN {VAL}",
                },
        {
            "id": "tg_heat_target",
            "name": "HEAT {VAL} (TARGET)",
                },
        {
            "id": "tg_line",
            "name": "LINE {VAL}",
                },
        {
            "id": "tg_cone",
            "name": "CONE {VAL}",
                },
        {
            "id": "tg_blast",
            "name": "BLAST {VAL}",
                },
        {
            "id": "tg_burst",
            "name": "BURST {VAL}",
                },
        {
            "id": "tg_accurate",
            "name": "ACCURATE",
                },
        {
            "id": "tg_arcing",
            "name": "ARCING",
                },
        {
            "id": "tg_ap",
            "name": "ARMOR-PIERCING (AP)",
                },
        {
            "id": "tg_inaccurate",
            "name": "INACCURATE",
                },
        {
            "id": "tg_knockback",
            "name": "KNOCKBACK {VAL}",
                },
        {
            "id": "tg_loading",
            "name": "LOADING",
                },
        {
            "id": "tg_ordnance",
            "name": "ORDNANCE",
                },
        {
            "id": "tg_overkill",
            "name": "OVERKILL",
                },
        {
            "id": "tg_overshield",
            "name": "OVERSHIELD",
                },
        {
            "id": "tg_reliable",
            "name": "RELIABLE {VAL}",
                },
        {
            "id": "tg_seeking",
            "name": "SEEKING",
                },
        {
            "id": "tg_smart",
            "name": "SMART",
                },
        {
            "id": "tg_threat",
            "name": "THREAT {VAL}",
                },
        {
            "id": "tg_thrown",
            "name": "THROWN {VAL}",
                },
        {
            "id": "tg_turn",
            "name": "{VAL}/TURN",
                },
        {
            "id": "tg_round",
            "name": "{VAL}/ROUND",
                },
        {
            "id": "tg_ai",
            "name": "AI",
                },
        {
            "id": "tg_danger_zone",
            "name": "DANGER ZONE",
                },
        {
            "id": "tg_deployable",
            "name": "DEPLOYABLE",
                },
        {
            "id": "tg_drone",
            "name": "DRONE",
                },
        {
            "id": "tg_full_action",
            "name": "FULL ACTION",
                },
        {
            "id": "tg_grenade",
            "name": "GRENADE",
                },
        {
            "id": "tg_heat_self",
            "name": "HEAT {VAL} (SELF)",
                },
        {
            "id": "tg_limited",
            "name": "LIMITED {VAL}",
                },
        {
            "id": "tg_mine",
            "name": "MINE",
                },
        {
            "id": "tg_mod",
            "name": "MOD",
                },
        {
            "id": "tg_protocol",
            "name": "PROTOCOL",
                },
        {
            "id": "tg_quick_action",
            "name": "QUICK ACTION",
                },
        {
            "id": "tg_reaction",
            "name": "REACTION",
                },
        {
            "id": "tg_shield",
            "name": "SHIELD",
                },
        {
            "id": "tg_unique",
            "name": "UNIQUE",
                },
        {
            "id": "tg_archaic",
            "name": "ARCHAIC",
                },
        {
            "id": "tg_personal_armor",
            "name": "PERSONAL ARMOR",
                },
        {
            "id": "tg_gear",
            "name": "GEAR",
                },
        {
            "id": "tg_sidearm",
            "name": "SIDEARM",
                },
        {
            "id": "tg_invade",
            "name": "INVADE",
            },
        {
            "id": "tg_quick_tech",
            "name": "QUICK TECH",
            },
        {
            "id": "tg_full_tech",
            "name": "FULL TECH",
            },
        {
            "id": "tg_free_action",
            "name": "FREE ACTION",
                },
        {
            "id": "tg_range",
            "name": "RANGE ({VAL})",
                },
        {
            "id": "tg_modded",
            "name": "MODDED",
            },
        {
            "id": "tg_resistance",
            "name": "RESISTANCE",
            },
        {
            "id": "tg_recharge",
            "name": "RECHARGE {VAL}+",
                },
        {
            "id": "tg_unlimited",
            "name": "UNLIMITED",
                },
        {
            "id": "tg_bonus_ai",
            "name": "AI",
                },
    ]
}