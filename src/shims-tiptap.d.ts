declare module 'tiptap' {
    import { Vue } from 'vue/types/vue'
  
    // const _a: any
    // export = _a
    // import * as Tiptap from 'tiptap'
    // export * from 'tiptap'
    // // export { Extension } from 'tiptap'
    // export type Extension = any
    // export type Editor = any
    // export type EditorMenuBubble = any
    type TiptapPlugin = Extension | Node | Mark | unknown
    export class Extension {
      constructor (...arg: any[])
      get name(): string
      get defaultOptions(): Record<string, any>
      get plugins(): Array<unknown>
      keys(context: ProseContext): Record<string, any>
      commands(context: ProseContext): Record<string, any>
      inputRules(context: ProseContext): Array<unknown>
      pasteRules(context: ProseContext): Array<unknown>
      get update(): (...args: any[]) => any
    }
    export interface UpdateContext {
      state: any
      getHTML: () => string
      getJSON: () => JSON
      transaction: any
    }
    export interface EditorOptions {
      editorProps?: Record<string, any>,
      editable?: boolean,
      autoFocus?: boolean,
      extensions?: TiptapPlugin[],
      content?: string,
      emptyDocument?: {
        type: string,
        content: [{
          type: string,
        }],
      },
      useBuiltInExtensions?: boolean,
      dropCursor?: Record<string, any>,
      onInit?: (context: any) => void,
      onTransaction?: (context: any) => void,
      onUpdate?: (context: UpdateContext) => void,
      onFocus?: (context: any) => void,
      onBlur?: (context: any) => void,
      onPaste?: (context: any) => void,
      onDrop?: (context: any) => void,
    }
    export class Editor {
      constructor (options: EditorOptions)
      setContent(content: JSON | string | HTMLElement, emitUpdate: boolean, options: Record<string, any>): void
      clearContent(emitUpdate: boolean): void
      setOptions(options: EditorOptions): void
      registerPlugin(plugin: Record<string, any>): void
      getJSON(): JSON
      getHTML(): HTMLElement
      focus(): void
      blur(): void
      destroy(): void
      content: Record<string, any> | String
      editorProps: Record<string, any>
      editable: boolean
      autoFocus: boolean
      extensions: Array<TiptapPlugin>
      useBuiltInExtensions: boolean
      dropCursor: Record<string, any>
      parseOptions: Record<string, any>
      onInit: FunctionConstructor
      defaultOptions: EditorOptions
      events: string[]
      builtInExtensions: Extension[]
      [key: string]: any
    }
    export class EditorMenuBubble extends Vue {
      [key: string]: any
    }
    export class EditorContent extends Vue {
      [key: string]: any
    }
    export class EditorMenuBar extends Vue {
      [key: string]: any
    }

    export interface ProseContext {
      type?: Record<string, any>, 
      schema?: Record<string, any>, 
      attrs?: Record<string, any>,
    }

    export class Node extends Vue {
      get name(): string
      get defaultOptions(): Record<string, any>
      get schema(): Record<string, any>
      get view(): Record<string, any>
      keys(context: ProseContext): Record<string, any>
      commands(context: ProseContext): Record<string, any>
      inputRules(context: ProseContext): Array<unknown>
      pasteRules(context: ProseContext): Array<unknown>
      get plugins(): Array<unknown>
    }
    export class Mark extends Vue {
      get name(): string
      get defaultOptions(): Record<string, any>
      get schema(): Record<string, any>
      get view(): Record<string, any>
      keys(context: ProseContext): Record<string, any>
      commands(context: ProseContext): Record<string, any>
      inputRules(context: ProseContext): Array<unknown>
      pasteRules(context: ProseContext): Array<unknown>
      get plugins(): Array<unknown>
    }

    export class Text extends Node {}
    export class Doc extends Node {}
    export class Paragraph extends Node {}
  
    // Heading,
    //   Bold,
    //   Italic,
    //   Strike,
    //   Underline,
    //   Code,
    //   CodeBlock,
    //   Paragraph,
    //   BulletList,
    //   OrderedList,
    //   ListItem,
    //   Blockquote,
    //   HardBreak,
    //   HorizontalRule,
    //   History,
    //   Link
  }

  declare module 'tiptap-extensions';