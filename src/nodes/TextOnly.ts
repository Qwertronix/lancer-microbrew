import { Node } from "tiptap";

export default class TextOnlyDoc extends Node {
    get name() {
        return 'doc'
    }
    get schema() {
        return {
            content: "text*",
        }
    }
}