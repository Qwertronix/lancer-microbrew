const PnpWebpackPlugin = require(`pnp-webpack-plugin`);

module.exports = {
  configureWebpack: {
    resolve: {
      plugins: [
        // PnpWebpackPlugin
        require('tailwindcss'),
        require('autoprefixer'),
      ]
    },
    resolveLoader: {
      plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
        // PnpWebpackPlugin.moduleLoader(module)
      ]
    }
  }
};